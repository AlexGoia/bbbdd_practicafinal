--1.- Obt�n el nombre de los pueblos, poblaci�n, extensi�n y densidad de poblaci�n (la densidad se ha de calcular).
SELECT NOM, POBLACIO, EXTENSIO, (POBLACIO/EXTENSIO) DENSITAT
FROM POBLACIONS;

-- 2.- Saca las distintas localidades donde haya institutos (cada localidad solo ha de salir una vez).
SELECT DISTINCT POB.NOM
FROM POBLACIONS POB, INSTITUTS INST
WHERE POB.NOM = INST.LOCALITAT
ORDER BY POB.NOM ASC;

-- 3.- Saca las poblaciones de habla valenciana que est�n a m�s de 1.000 de altura.
SELECT *
FROM POBLACIONS
WHERE LLENGUA='V' AND ALTURA>1000
ORDER BY NOM ASC;

--4.- Saca las poblaciones de una densidad de poblaci�n de m�s de 200 h/Km2 y una altura de m�s de 500 m.
SELECT NOM, POBLACIO, EXTENSIO, (POBLACIO/EXTENSIO) DENSITAT, ALTURA
FROM POBLACIONS
WHERE (POBLACIO/EXTENSIO) > 200
AND ALTURA > 500
ORDER BY NOM ASC;

-- 5.- Saca el n�mero de comarcas de cada provincia.
SELECT PROVINCIA, COUNT(*) NUM_COMARQUES
FROM COMARQUES
GROUP BY PROVINCIA;

-- 6.- Saca todas las poblaciones, ordenadas por comarca, y dentro de la misma comarca por nombre.
SELECT POB.NOM, POB.NOM_C
FROM POBLACIONS POB
ORDER BY POB.NOM_C, POB.NOM

-- 7.- Saca las poblaciones de la comarca de l'Alt Maestrat, ordenadas por altura de forma descendente.
SELECT PO.NOM, PO.NOM_C, PO.ALTURA
FROM POBLACIONS PO
WHERE UPPER(PO.NOM_C) = 'ALT MAESTRAT'
ORDER BY PO.ALTURA DESC;

-- AGRUPACIONES --
-- 1.- Calcular el n�mero de Institutos de cada poblaci�n.
-- Solamente obtener aquellas poblaciones que tienen institutos
SELECT INST.LOCALITAT, COUNT(*)
FROM INSTITUTS INST, POBLACIONS POB
WHERE INST.LOCALITAT = POB.NOM
GROUP BY INST.LOCALITAT 
ORDER BY INST.LOCALITAT ASC;

-- 2.- Saca las comarcas de m�s de 100.000 habitantes.
SELECT PO.NOM_C, SUM(PO.POBLACIO) TOTAL_HABITANTS
FROM POBLACIONS PO
GROUP BY PO.NOM_C
HAVING SUM(PO.POBLACIO)>100000
ORDER BY PO.NOM_C ASC;
 
-- 3.- Saca la densidad de poblaci�n de cada comarca, ordenada por esta de forma descendente.
SELECT POB.NOM_C, ROUND(SUM(POB.POBLACIO)/SUM(POB.EXTENSIO),2) DENSITAT
FROM POBLACIONS POB
GROUP BY POB.NOM_C
ORDER BY DENSITAT DESC;

-- 4.- Cuenta cuantas lenguas son originarias de cada comarca.
SELECT NOM_C, COUNT(*)
FROM(SELECT NOM_C, LLENGUA
      FROM POBLACIONS
      GROUP BY NOM_C, LLENGUA
      ORDER BY NOM_C)
GROUP BY NOM_C
ORDER BY NOM_C ASC

--Inner/Outer Join
--1.- Saca todas las poblaciones, ordenadas por provincia, dentro de la misma 
--provincia por comarca, y dentro de cada comarca por nombre de la poblaci�n.
SELECT PO.NOM, PO.NOM_C, CO.PROVINCIA
FROM POBLACIONS PO, COMARQUES CO
WHERE PO.NOM_C = CO.NOM_C
ORDER BY CO.PROVINCIA, PO.NOM_C, PO.NOM

--2.- Saca el n�mero de habitantes de cada poblaci�n y el n�mero de los institutos 
--(incluyendo los que no tienen).
SELECT PO.NOM, PO.POBLACIO, COUNT(CODI)
FROM POBLACIONS PO, INSTITUTS INSTI
WHERE PO.NOM=INSTI.LOCALITAT(+) -- datos que puede que no tengan
GROUP BY PO.NOM, PO.POBLACIO, INSTI.LOCALITAT
ORDER BY PO.NOM ASC

--3.- Saca el n�mero de habitantes por instituto de aquellas poblaciones que tienen instituto.
SELECT PO.NOM, ROUND((PO.POBLACIO/COUNT(*)),2) HAB_POR_INSTITUTO
FROM POBLACIONS PO, INSTITUTS INSTI
WHERE PO.NOM=INSTI.LOCALITAT
GROUP BY PO.NOM, PO.POBLACIO 
--DADO QUE POBLACIO SIEMPRE SER� IGUAL SI HACEMOS ESTO PODEMOS AGRUPAR Y
-- USAR LA POBLACIO EN LA SELECT
ORDER BY PO.NOM ASC;

--4.- Saca cu�tos pueblos hablan valenciano en cada comarca, indicando tambi�n la provincia.
SELECT PO.NOM_C, COUNT(*), CO.PROVINCIA
FROM POBLACIONS PO,
COMARQUES CO
WHERE PO.LLENGUA='V' AND
PO.NOM_C = CO.NOM_C
GROUP BY PO.NOM_C, CO.PROVINCIA
ORDER BY PO.NOM_C ASC

-- 5.- Saca las comarcas y el n�mero de los institutos de cada comarca, 
-- incluyento aquellos que no tienen ninguno.
SELECT PO.NOM_C, COUNT(INSTI.NOM)
FROM POBLACIONS PO, INSTITUTS INSTI
WHERE PO.NOM=INSTI.LOCALITAT(+)
GROUP BY PO.NOM_C
ORDER BY PO.NOM_C ASC;

-- SUBCONSULTAR
-- 1.- Saca los pueblos con m�s habitantes que la media.
SELECT NOM, POBLACIO
FROM POBLACIONS
WHERE POBLACIO > (
    SELECT AVG(POBLACIO)
    FROM POBLACIONS -- MEDIA
    )
ORDER BY NOM ASC;

-- 2.- Saca toda la informaci�n del pueblo con menos habitantes que tenga instituto
SELECT *
FROM POBLACIONS PO, INSTITUTS INSTI
WHERE PO.POBLACIO = (
    SELECT MIN(POBLACIO)
    FROM POBLACIONS PO, 
    INSTITUTS INSTI
    WHERE PO.NOM = INSTI.LOCALITAT
) 
AND PO.NOM=INSTI.LOCALITAT

--3.- Saca los pueblos de m�s de 2.000 h. que no tengan instituto.
SELECT NOM, POBLACIO
FROM POBLACIONS PO
WHERE POBLACIO > 2000
AND NOT EXISTS (
    SELECT 0
    FROM INSTITUTS
    WHERE LOCALITAT=PO.NOM
)
ORDER BY NOM ASC;

--4.- Saca cu�ntos pueblos hablan valenciando en cada Comarca, incluso aquellos que no tienen ninguno.
--Realizala de dos maneras:
--a) Sin subconsultas con outer join 
SELECT * FROM INSTI.COMARQUES;

SELECT * FROM INSTI.INSTITUTS;

SELECT * FROM INSTI.POBLACIONS;

SELECT PUEBLOS.NOM_C
FROM INSTI.POBLACIONS PUEBLOS, INSTI.COMARQUES COMARCAS
WHERE PUEBLOS.LLENGUA = 'V'
AND COMARCAS.NOM_C = PUEBLOS.NOM_C


-- VOLVER DESPU�S DE HACER EL DE STARWARS



--b) Con una select simple de Comarques y usando una subconsulta en la propia l�nea de la select
SELECT (
    SELECT NOM_C
    FROM INSTI.COMARQUES
)
FROM INSTI.POBLACIONS
WHERE INSTI.POBLACIONS.LLENGUA!='V';
WHERE NOM_C=NOM-C_PO(+)
AND LENGUA(+) ='V'

--Nota: Poner una subconsulta en la misma l�nea select de su consulta principal es algo poco habitual y no demasiado recomendable, pero a veces es �til.

-- 5.- Saca las comarcas que tienen m�s institutos que la media.
SELECT NOM_C, COUNT(*) NUM_INSTITUTS
FROM POBLACIONS POB, INSTITUTS INS
WHERE POB.NOM=INS.LOCALITAT
GROUP BY NOM_C
HAVING COUNT(*) > (
    SELECT ROUND(AVG(COUNT(*)),2)
    FROM POBLACIONS PO, INSTITUTS INSTI
    WHERE PO.NOM=INSTI.LOCALITAT
    GROUP BY PO.NOM_C
    )
ORDER BY NOM_C;