
CREATE TABLE CABALLOS (
    CODCABALLO VARCHAR2(4) NOT NULL,
    NOMBRE VARCHAR2(20),
    PESO NUMBER(3),
    fechanacimiento DATE,
    PROPIETARIO VARCHAR2(25),
    NACIONALIDAD VARCHAR2(20),
    CONSTRAINT PK_CABALLOS PRIMARY KEY(CODCABALLO),
    CONSTRAINT CHECK_NOMBRE CHECK(NOMBRE IS NOT NULL),
    CONSTRAINT CHECK_PESO CHECK(PESO BETWEEN 240 AND 300),
    CONSTRAINT CHECK_FECHA CHECK(TRUNC(fechanacimiento)>TRUNC(TO_DATE('1-1-2000','DD-MM-YYYY')))
)

CREATE TABLE CARRERAS ( 
    codcarrera VARCHAR2(4) NOT NULL,
    fechahora DATE,
    NOMBRE VARCHAR2(20),
    importepremio NUMBER(6),
    apuestalimite NUMBER(7,2),
    CONSTRAINT PK_CARRERAS PRIMARY KEY(codcarrera),
    -- CONSTRAINT CHECK_HORA CHECK(fechahora BETWEEN TO_DATE('09:30','HH24:MI') AND TO_DATE('14:30','HH24:MI')),
    -- PREGUNTAR
    CONSTRAINT CHECK_APUESTALIMITE CHECK(apuestalimite<20000)
)

CREATE TABLE CLIENTES (
    dnicliente VARCHAR2(10) NOT NULL,
    NOMBRE VARCHAR2(20),
    NACIONALIDAD VARCHAR2(20),
    CONSTRAINT PK_CLIENTES PRIMARY KEY(dnicliente)
)

CREATE TABLE APUESTAS (
    dnicliente VARCHAR2(10) NOT NULL,
    CODCABALLO VARCHAR2(4) NOT NULL,
    codcarrera VARCHAR2(4) NOT NULL,
    IMPORTE NUMBER(6) DEFAULT 300,
    TANTOPORUNO NUMBER(4,2),
    CONSTRAINT PK_APUESTAS PRIMARY KEY(dnicliente, CODCABALLO, codcarrera),
    CONSTRAINT FK_APUESTAS_CLIENTE FOREIGN KEY(dnicliente) REFERENCES CLIENTES(dnicliente),
    CONSTRAINT FK_APUESTAS_CABALLOS FOREIGN KEY(CODCABALLO) REFERENCES CABALLOS(CODCABALLO),
    CONSTRAINT FK_APUESTAS_CARRERA FOREIGN KEY(codcarrera) REFERENCES CARRERAS(codcarrera),
    CONSTRAINT CHECK_IMPORTE CHECK(IMPORTE IS NOT NULL),
    CONSTRAINT CHECK_TANTOPORUNO CHECK(TANTOPORUNO>1)
)

CREATE TABLE PARTICIPACIONES (
    CODCABALLO VARCHAR2(4) NOT NULL,
    codcarrera VARCHAR2(4) NOT NULL,
    DORSAL NUMBER(2),
    JOCKEY VARCHAR2(20),
    posicionfinal NUMBER(2),
    CONSTRAINT PK_PARTICIPACIONES PRIMARY KEY(CODCABALLO,codcarrera),
    CONSTRAINT CHECK_DORSAL CHECK(DORSAL IS NOT NULL),
    CONSTRAINT CHECK_JOCKEY CHECK(JOCKEY IS NOT NULL),
    CONSTRAINT CHECK_posicionfinal CHECK(posicionfinal > 0),
    CONSTRAINT FK_PARTICIPACIONES_CABALLOS FOREIGN KEY(CODCABALLO) REFERENCES CABALLOS(CODCABALLO),
    CONSTRAINT FK_PARTICIPACIONES_CARRERAS FOREIGN KEY(codcarrera) REFERENCES CARRERAS(codcarrera)
)

--Indica el nombre de los caballos, el nombre de los jinetes y la posici�n, de aquellos que
--hayan quedado entre los dos primeros puestos y que sean de nacionalidad Brit�nica
--ordenados por el nombre del caballO

SELECT CAB.NOMBRE, PART.JOCKEY, PART.POSICIONFINAL
FROM CABALLOS CAB, PARTICIPACIONES PART
WHERE CAB.CODCABALLO = PART.CODCABALLO
AND PART.POSICIONFINAL BETWEEN 1 AND 2
AND CAB.NACIONALIDAD = 'Brit�nica'
ORDER BY CAB.NOMBRE;

--Ejercicio 2
--Nombre de los Caballos de nacionalidad Espa�ola que tengan una A en su nombre o
--Caballos que los clientes que ha apostado por ellos empiecen con consonante, saliendo
--duplicados, a lo que te de quit�ndole los Caballos que hayan quedado segundo. Debes
--usar conjuntos.
SELECT CAB.NOMBRE
FROM CABALLOS CAB
WHERE upper(CAB.NACIONALIDAD)='ESPA�OLA'
AND INSTR(UPPER(CAB.NOMBRE),'A')<>0
UNION ALL
SELECT CAB1.NOMBRE
FROM CABALLOS CAB1, APUESTAS AP
WHERE CAB1.CODCABALLO = AP.CODCABALLO
AND (UPPER(CAB1.NOMBRE) NOT LIKE 'A%' OR UPPER(CAB1.NOMBRE) NOT LIKE 'E%' OR 
UPPER(CAB1.NOMBRE) NOT LIKE 'I%' OR UPPER(CAB1.NOMBRE) NOT LIKE 'O%' OR 
UPPER(CAB1.NOMBRE) NOT LIKE 'U%')
MINUS
SELECT CAB2.NOMBRE
FROM CABALLOS CAB2, PARTICIPACIONES PAR
WHERE CAB2.CODCABALLO = PAR.CODCABALLO
AND PAR.POSICIONFINAL = 2;

--
--Ejercicio 3
--Nos ordenan eliminar las carreras en las que no hay caballos inscritos
DELETE CARRERAS CA
WHERE NOT EXISTS(
    SELECT 1
    FROM PARTICIPACIONES PA
    WHERE PA.CODCARRERA = CA.CODCARRERA
)

--Ejercicio 4
--Como podr�s comprobar en la Carrera C6 no hay caballos para realizarla. Introduce 4
--caballos de lo ya existentes en la carrera C6. Te puedes inventar jockeys y dorsales.

-- ANTES NOS HAS PEDIDO BORRAR LA CARRERA C6, AS� QUE LA VOLVER� A INTRODUCIR:
ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI';

INSERT INTO carreras(codcarrera, fechahora, importepremio, apuestalimite)
VALUES('C6', TO_DATE('21/10/2009 11:00'), 5000, 400);

INSERT INTO PARTICIPACIONES(CODCABALLO, CODCARRERA, DORSAL, JOCKEY)
VALUES('3','C6','16','Pepe el rel�mpago')

INSERT INTO PARTICIPACIONES(CODCABALLO, CODCARRERA, DORSAL, JOCKEY)
VALUES('4','C6','19','Jose el bala')

INSERT INTO PARTICIPACIONES(CODCABALLO, CODCARRERA, DORSAL, JOCKEY)
VALUES('5','C6','22','Juan el campe�n')

INSERT INTO PARTICIPACIONES(CODCABALLO, CODCARRERA, DORSAL, JOCKEY)
VALUES('6','C6','13','Jorge el vencido')
    
--Ejercicio 5
--Ya se ha celebrado la carrera C6 y los caballos llegan por orden de edad del m�s joven
--al m�s adulto.
--Actualiza la tabla Participaciones con los resultados de la carrera.

UPDATE PARTICIPACIONES P
SET POSICIONFINAL =
    (SELECT COUNT(*)+1
        FROM CABALLOS
        WHERE FECHANACIMIENTO < (
                SELECT FECHANACIMIENTO
                FROM CABALLOS
                WHERE CODCABALLO = P.CODCABALLO
            )
        AND CODCABALLO IN (
                SELECT CODCABALLO
                FROM PARTICIPACIONES
                WHERE CODCARRERA='C6'
            )
)
WHERE CODCARRERA='C6';

--Ejercicio 6
--A�ade los siguientes CHECKS a las tablas del modelo:
--a) En las Participaciones los nombres de los jockeys tienen siempre las iniciales en
--may�sculas.
ALTER TABLE PARTICIPACIONES ADD CONSTRAINT CHECK_JOKEY_NAME CHECK(
JOCKEY = INITCAP(JOCKEY)
) -- PREGUNTAR 

--b) La nacionalidad de los caballos solo puede ser Brit�nica, Espa�ola, Macedonia o
--�rabe.
ALTER TABLE CABALLOS ADD CONSTRAINT CHECK_NACONALIDAD CHECK (
    NACIONALIDAD IN ('Brit�nica','Espa�ola','Macedonia','�rabe')
)

--c) La temporada de carreras solo puede transcurrir del 10 de Marzo al 10 de
--Noviembre.

ALTER TABLE CARRERAS ADD CONSTRAINT CHECK_TEMPORADA CHECK(
   TRUNC(FECHAHORA,'DD/MM') BETWEEN TO_DATE('10/03','DD/MM') AND TO_DATE('10/11','DD/MM')
) -- PREGUNTAR

--Ejercicio 7
--Saca de cada una de las carreras el n�mero total de participantes, debes incluir aquellas
--en las que no hay caballos inscritos.
SELECT CA.CODCARRERA, COUNT(PA.CODCARRERA) PARTICIPANTES
FROM PARTICIPACIONES PA, CARRERAS CA
WHERE CA.CODCARRERA=PA.CODCARRERA(+)
GROUP BY CA.CODCARRERA
ORDER BY CA.CODCARRERA
-- CORRECTO

--Ejercicio 8
--Te piden que introduzcas la siguiente apunesta, un cliente con nacionalidad Escocesa
--ha apostado por el caballo m�s pesado de la primera carrera que se corra duante el
--verano del 2009 con un importe de 2000, y sabemos que en ese momento para esa
--carrera a ese caballo se paga 30 a 1

SELECT CLI.DNICLIENTE
FROM CLIENTES CLI
WHERE CLI.NACIONALIDAD = 'Escocesa' -- en este caso devuelve un resultado,
-- pero deber�a especificarse alg�n dato m�s para hacer que siempre
-- devuelva un resultado

SELECT DISTINCT CABA.CODCABALLO
FROM CABALLOS CABA, PARTICIPACIONES PA, CARRERAS CAR
WHERE CABA.CODCABALLO = PA.CODCABALLO AND
PA.CODCARRERA = CAR.CODCARRERA
AND CAR.FECHAHORA BETWEEN TO_DATE('20/06/2009','DD/MM/YYYY') AND TO_DATE('21/09/2009','DD/MM/YYYY')
AND CABA.PESO = SELECT(
    MAX(PESO)
)

--Ejercicio 9
--Obt�n el nombre del caballo y la carrera con m�s peso en cada carrera
SELECT  CRR.CODCARRERA
FROM CABALLOS CBLL, PARTICIPACIONES PRT, CARRERAS CRR
WHERE CBLL.CODCABALLO = PRT.CODCABALLO AND PRT.CODCARRERA = CRR.CODCARRERA
AND CBLL.PESO IN (
    SELECT MAX(CBLL.PESO)
    FROM CABALLOS CBLL2, PARTICIPACIONES PRT2
    WHERE CBLL2.CODCABALLO = PRT2.CODCABALLO AND PRT2.CODCARRERA = CRR.CODCARRERA
    GROUP BY CRR.CODCARRERA
    )
GROUP BY CRR.CODCARRERA                
--Ejercicio 10
--Teniendo en cuenta estas dos premisas:
--1) Un cliente gana una apuesta si el caballo por el que apuesta gana la carrera.
--2) El importe de su premio es el importe apostado multiplicado por el tanto por uno de
--la apuesta correspondiente.
--Muestra los nombres de todos los clientes que con sus apuestas hayan ganado m�s de
--8000 euros


--Ejercicio 11
--Dime jockey que m�s carreras ha ganado
SELECT JOCKEY
FROM PARTICIPACIONES
WHERE POSICIONFINAL=1
GROUP BY JOCKEY
HAVING COUNT(*) =(
        SELECT MAX(COUNT(*))
        FROM PARTICIPACIONES
        WHERE POSICIONFINAL=1
        GROUP BY JOCKEY
    )
-- CORRECTO

--Ejercicio 12
--Sabiendo que el premio de una carrera se lo lleva el vencedor de la misma, dime el
--propietario que m�s dinero ha ganado en premios con sus caballos

SELECT CBLL.PROPIETARIO
FROM 
CABALLOS CBLL,
PARTICIPACIONES PTR,
CARRERAS CRR
WHERE PTR.CODCARRERA = CRR.CODCARRERA
AND PTR.CODCABALLO = CBLL.CODCABALLO
AND PTR.POSICIONFINAL = 1 -- SE REPITE LA CONDICI�N TAMBI�N EN LA SELECT
GROUP BY CBLL.PROPIETARIO
HAVING SUM(NVL(CRR.IMPORTEPREMIO,0)) = (
    SELECT MAX(SUM(NVL(CRR2.IMPORTEPREMIO,0)))
    FROM 
    CABALLOS CBLL2,
    PARTICIPACIONES PTR2,
    CARRERAS CRR2
    WHERE PTR2.CODCARRERA = CRR2.CODCARRERA
    AND PTR2.CODCABALLO = CBLL2.CODCABALLO
    AND PTR2.POSICIONFINAL = 1 -- SE REPITE TAMBI�N AQU� POR QUE DEBE RESTRINGIR LO MISMO
    GROUP BY CBLL2.PROPIETARIO
);
-- CORRECTO

--Ejercicio 13
--Sacas los caballos (sus nombres) que m�s veces han quedado entre los tres primeros
--puestos.
SELECT CBLL.NOMBRE
FROM CABALLOS CBLL
WHERE CBLL.CODCABALLO IN (
    SELECT PTR.CODCABALLO
    FROM PARTICIPACIONES PTR
    WHERE PTR.POSICIONFINAL IN (1,2,3)
    GROUP BY PTR.CODCABALLO
    HAVING COUNT(*)=(
        SELECT MAX(COUNT(*))
        FROM PARTICIPACIONES PTR
        WHERE PTR.POSICIONFINAL IN (1,2,3)
        GROUP BY PTR.CODCABALLO
    )
)
-- CORRECTO

-- Ejercicio 14
-- Obt�n los clientes y la cantidad de caballos sobre los que han apostado,
-- teniendo en cuenta que deben
-- haber apostado a m�s de 1 caballo y con una suma de todas sus apuestas de m�s de
-- 3.000 euros.
    SELECT CLI.NOMBRE, COUNT(*), SUM(AP.IMPORTE)
    FROM CLIENTES CLI, APUESTAS AP
    WHERE CLI.DNICLIENTE = AP.DNICLIENTE
    GROUP BY CLI.NOMBRE
    HAVING COUNT(*)>1 
    AND SUM(AP.IMPORTE)> 3000