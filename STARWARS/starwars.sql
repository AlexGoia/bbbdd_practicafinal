-- 1- Arreglar ACTORES. Hay muchos actores repetidos, arreglarlo, para ello tenemos que eliminar de
--todos los actores que salgan m�s de 1 vez y nos vamos a quedar solo con 1. El problema que ACTORES
--hay 2 FK que apuntan a esta tabla, PERSONAJES y HACER_CASTING. Por lo tanto de todo lo que
--est� mal vamos a tener 3 posibilidades.

-- a)Primero crea una tabla temporal con todos los ACTORES repetidos que nos servir� de BACKUP
--(todos los datos de ACTORES)

CREATE TABLE BACKUP (
    NSS VARCHAR2(12) NOT NULL,
    NOMBRE VARCHAR2(40),
    EDAD NUMBER(3),
    SEXO VARCHAR2(20),
    NACIONALIDAD VARCHAR2(20),
    FECHA_NAC DATE
)

ALTER TABLE BACKUP
    ADD CONSTRAINT PK_BACKUP PRIMARY KEY (NSS)


INSERT INTO BACKUP 
    SELECT *
    FROM ACTORES 
    WHERE NOMBRE IN (SELECT NOMBRE
                FROM ACTORES
                GROUP BY 
                NOMBRE
                HAVING COUNT(*)>1)

--b) Ahora vamos a ver que tenemos mal, saca los ACTORES repetidos y cuantos hay
SELECT NOMBRE, COUNT(*)
FROM ACTORES
GROUP BY NOMBRE
HAVING COUNT(*)>1

--c) Caso 1: El m�s sencillo, el caso que los actores repetidos no est�n en ninguna de las de las tablas
--PERSONAJES ni HACER CASTING. Saca los ACTORES repetidos que no est�n en
--HACER_CASTING ni en PERSONAJES, atenci�n NINGUNO de los repetidos deben estar, es
--decir, que puede de los repetidos y unos est�n en estas tablas y otros no, esta select SOLO quiero los
--que NINGUNO este.
SELECT *
FROM ACTORES ACT
WHERE NOMBRE IN (SELECT NOMBRE
                FROM ACTORES
                GROUP BY 
                NOMBRE
                HAVING COUNT(*)>1)
AND NOT EXISTS(
    SELECT 1
    FROM HACER_CASTING HC
    WHERE HC.NSS = ACT.NSS
)
AND NOT EXISTS(
    SELECT 1
    FROM PERSONAJES PRSNJ
    WHERE PRSNJ.NSS_ACTOR = ACT.NSS
)
ORDER BY NOMBRE ASC;

--d) Arreglar Caso 1: Elimina los actores repetidos dejando los que tengan el nss m�nimo por cada uno de
--ellos que no est�n en HACER_CASTING ni en PERSONAJES.
-- COMMIT
DELETE ACTORES
WHERE NSS IN (
        SELECT NSS
        FROM ACTORES ACT
        WHERE NOMBRE IN (SELECT NOMBRE
                        FROM ACTORES
                        GROUP BY
                        NOMBRE
                        HAVING COUNT(*)>1
                        )
        AND NOT EXISTS(
            SELECT 1
            FROM HACER_CASTING HC
            WHERE HC.NSS = ACT.NSS
        )
        AND NOT EXISTS(
            SELECT 1
            FROM PERSONAJES PRSNJ
            WHERE PRSNJ.NSS_ACTOR = ACT.NSS
        )
        AND NSS > (
            SELECT MIN(NSS)
            FROM ACTORES A2
            WHERE ACT.NOMBRE = A2.NOMBRE
        )
    )
--Date cuenta que cuando cruces con el padre todo lo que haya arriba ya lo cumples, por lo que la select del MIN es bastante trivial.

--e) Caso 2: Ahora saca una select de los ACTORES que est�n repetidos y que unos est�n en HACER_CASTING o en
--PERSONAJES y otros no, 
SELECT *
FROM ACTORES ACT
WHERE NOMBRE IN (SELECT NOMBRE
                FROM ACTORES
                GROUP BY 
                NOMBRE
                HAVING COUNT(*)>1)
AND (EXISTS(
        SELECT 1
        FROM HACER_CASTING HC
        WHERE HC.NSS = ACT.NSS
    )
OR EXISTS(
        SELECT 1
        FROM PERSONAJES PRSNJ
        WHERE PRSNJ.NSS_ACTOR = ACT.NSS
))
AND(NOT EXISTS(
            SELECT 1
            FROM HACER_CASTING HC
            WHERE HC.NSS = ACT.NSS
        )
        OR NOT EXISTS(
            SELECT 1
            FROM PERSONAJES PRSNJ
           WHERE PRSNJ.NSS_ACTOR = ACT.NSS
     ))
ORDER BY NOMBRE ASC;

--f) Arreglar Caso 2: en ese caso elimina todos los que no est�n
DELETE ACTORES
WHERE NSS IN (
    SELECT NSS
   FROM ACTORES ACT
   WHERE NOMBRE IN (SELECT NOMBRE
                    FROM ACTORES
                    GROUP BY 
                    NOMBRE
                    HAVING COUNT(*)>1)
    AND NOT EXISTS(
           SELECT 1
            FROM HACER_CASTING HC
            WHERE HC.NSS = ACT.NSS
        )
    OR NOT EXISTS(
            SELECT 1
            FROM PERSONAJES PRSNJ
           WHERE PRSNJ.NSS_ACTOR = ACT.NSS
    )
);


--2- Arreglar Localizaciones
--a) Primero vamos a ver que tenemos mal, saca las LOCALIZACIONES repetidas y cuantas hay.
SELECT LOC.NOMBRE_LOC, COUNT(*)
FROM LOCALIZACIONES LOC
GROUP BY LOC.NOMBRE_LOC
HAVING COUNT(*)>1 -- REPETIDO

--b) Actualiza el nombre de estas localizaciones repetidas concatenando al nombre que ya tienen los
--primeros 50 caracteres del campo SOLUCION de la tabla ACTUACION (en caso de ser
--NULO, coge los de PROBLEMA, y si los dos son NULO coge el c�digo de localizaci�n) que
--tenga asociada dicha localizaci�n, como puede haber varios, que de los asociados a dicha
--localizaci�n tengan el que su temporada y capitulo concatenados sean el m�s peque�o . Y si a�n
--as� hay m�s de uno, de esos que haya te quedar�s con el que tenga m�nimo nombre de
--personaje.
UPDATE LOCBACKUP LOC
SET NOMBRE_LOC = CONCAT(
    NOMBRE_LOC, 
    SUBSTR(
            NVL(
                (SELECT SOLUCION FROM ACTUAR ACT WHERE ACT.COD_LOC = LOC.COD_LOC),
                NVL(
                    'SELECT_PROBLEMA',
                    'COD_LOCALIZACION')
               )
    ,1,50)
)
WHERE NOMBRE_LOC IN (
    SELECT LOC.NOMBRE_LOC
    FROM LOCBACKUP LOC
    GROUP BY LOC.NOMBRE_LOC
    HAVING COUNT(*)>1
)

/* EJERCICIOS */
--EJERCICIOS MODELO STAR WARS
--1- Actores que tengan todas las vocales en su nombre
SELECT ACT.NOMBRE
FROM ACTORES ACT
WHERE INSTR(ACT.NOMBRE,'A')<>0
AND INSTR(ACT.NOMBRE,'E')<>0
AND INSTR(ACT.NOMBRE,'I')<>0
AND INSTR(ACT.NOMBRE,'O')<>0
AND INSTR(ACT.NOMBRE,'U')<>0

--2- Actores nacidos antes de 1960 y que tengan menos de 4 letras en su nombre.
SELECT ACT.NOMBRE, ACT.FECHA_NAC
FROM ACTORES ACT
WHERE ACT.FECHA_NAC<TO_DATE('01-01-1960','DD-MM-YYYY')
AND LENGTH(ACT.NOMBRE)<4

--3- Haz una vista con los Soldados con m�s de 5 a�os de experiencia, la vista debe impedir que se
--pueda actualizar si incumple la condici�n de creaci�n. Debes comprobarlo
CREATE VIEW SOLDADO_SENIOR
AS
SELECT SL.NOMBRE_SOLDADO, SL.ANYOS_EXP, SL.TIPO_ARMA
FROM SOLDADO SL
WHERE SL.ANYOS_EXP>5
WITH CHECK OPTION;
 
--4- A�ade un campo a personaje que tenga un car�cter alfanum�rico, y actualiza este campo a S si el
--personaje tiene un reclutador, y N si no es as�.
ALTER TABLE PERSONAJES 
ADD TIENE_RECLUTADOR CHAR;

UPDATE PERSONAJES
SET TIENE_RECLUTADOR = DECODE(PERSONAJES.NOMBRE_RECLUTADOR, NULL, 'N','S')

--5- Elimina el campo anterior de la tabla
ALTER TABLE PERSONAJES
DROP COLUMN TIENE_RECLUTADOR;

--6- Vista no actualizable de las Localizaciones visitadas en la 2 temporada


--7- Personajes que hayan participado en alguna pel�cula con la palabra Star Wars (no se puede usar
--like). Tambi�n deben salir las posiciones del 5 al 10 de ese t�tulo y en min�sculas.
SELECT PRS.NOMBRE, PE.TITULO, LOWER(SUBSTR(PE.TITULO,5,10))
FROM PERSONAJES PRS, ACTUAR ACT, 
CAPITULOS CP, PELICULAS PE
WHERE PRS.NOMBRE=ACT.NOM_PERSONAJE
AND ACT.CAPITULO=CP.NUM_CAP
AND ACT.TEMPORADA=CP.NUM_TEMPO
AND CP.NUM_TEMPO=PE.NUM_TEMPORADA
AND INSTR(UPPER(PE.TITULO),'STAR WARS')<>0
GROUP BY PRS.NOMBRE, PE.TITULO -- ELIMINAMOS DUPLICADOS
ORDER BY PRS.NOMBRE

--8- Cantidad y suma de todos los derribos efectuados por Rebeldes.
-- ATENCI�N ERROR DE DISE�O
-- HAY DIFERENTES VALORES EN DERRIBAR, AS� QUE VAMOS A ARREGLARO
-- R QUIERE QUE HA SIDO DERRIBADO UN REBELDE
-- I QUIERE DECIR QUE HA SIDO DERRIBADO UN IMPERIAL
-- arreglos:
UPDATE DERRIBAR
SET DERRIBADO='R'
WHERE DERRIBADO='0'
OR DERRIBADO='N'
OR DERRIBADO='M'

UPDATE DERRIBAR
SET DERRIBADO='I'
WHERE DERRIBADO='1'
OR DERRIBADO='S'
OR DERRIBADO='2'

-- soluci�n

SELECT COUNT(*) DERRIBOS_REBELDES
FROM DERRIBAR DB
WHERE DB.DERRIBADO = 'I' -- HA SIDO DERRIBADO EL IMPERIAL (EL REBELDE HA DERRIBADO)
GROUP BY DB.DERRIBADO

-- EN ESTE DISE�O NO TIENE SENTIDO HABLAR DE SUMA Y DE CANTIDAD...

--9- Obt�n cada personaje Rebelde y a la derecha que aparezca la palabra CONVERTIDO o NO
--CONVERTIDO, si lo ha sido o no.
SELECT R.NOMBRE_REB NOMBRE, DECODE(COUNT(C.NOM_JEDI),0,'NO CONVERTIDO','CONVERTIDO') RESULTADO
FROM REBELDE R,JEDI J, CONVERTIR C
WHERE R.NOMBRE_REB=J.NOMBRE_JEDI(+)
AND J.NOMBRE_JEDI=C.NOM_JEDI(+)
GROUP BY R.NOMBRE_REB

--10- Saca cuantos diferentes niveles de Jedi tenemos en la base de datos (nivel y cantidad)
SELECT J.NIVEL_JEDI NIVEL, COUNT(*) CANTIDAD
FROM JEDI J
GROUP BY J.NIVEL_JEDI
ORDER BY J.NIVEL_JEDI

--11- Obt�n cu�ntas localizaciones tiene cada cap�tulo.
-- SOL1
SELECT ACT.CAPITULO, ACT.TEMPORADA, COUNT(*)
FROM ACTUAR ACT, LOCALIZACIONES LOC
WHERE ACT.COD_LOC = LOC.COD_LOC
GROUP BY ACT.CAPITULO, ACT.TEMPORADA
ORDER BY ACT.TEMPORADA ASC

-- SOL2
SELECT CP.TITULO_CAP, COUNT(*)
FROM CAPITULOS CP, ACTUAR ACT
WHERE CP.NUM_CAP=ACT.CAPITULO
AND CP.NUM_TEMPO=ACT.TEMPORADA
GROUP BY CP.TITULO_CAP -- UN CAPITULO ES EL NUMERO DEL CAPITULO Y LA TEMPORADA
ORDER BY COUNT(*) ASC

--12- Saca el piloto con m�s derribos.
-- NO LE VEO EL SENTIDO SEG�N EL DISE�O...
-- SOLUCION 1
SELECT MAX(NOMBRE), MAX(CANTIDAD)
FROM (
    SELECT DRB.NOM_REBELDE NOMBRE, COUNT(*) CANTIDAD
    FROM DERRIBAR DRB
    WHERE DRB.DERRIBADO = 'I'
    GROUP BY DRB.NOM_REBELDE
    HAVING COUNT(*) = (
        SELECT MAX(COUNT(*))
        FROM DERRIBAR
        WHERE DERRIBADO = 'I'
        GROUP BY NOM_REBELDE
    )
    UNION
    SELECT DRB.NOM_IMPERIAL NOMBRE, COUNT(*) CANTIDAD
    FROM DERRIBAR DRB
    WHERE DRB.DERRIBADO = 'R'
    GROUP BY DRB.NOM_IMPERIAL
    HAVING COUNT(*) = (
        SELECT MAX(COUNT(*))
        FROM DERRIBAR
        WHERE DERRIBADO = 'R'
        GROUP BY NOM_IMPERIAL
)
)

-- SOLUCION 2
SELECT DECODE(DRB.DERRIBADO,'R',DRB.NOM_IMPERIAL,'I',DRB.NOM_REBELDE) NOMBRE, COUNT(*) DERRIBOS
FROM DERRIBAR DRB
GROUP BY DECODE(DRB.DERRIBADO,'R',DRB.NOM_IMPERIAL,'I',DRB.NOM_REBELDE)
HAVING COUNT(*) = (
    SELECT MAX(COUNT(*))
    FROM DERRIBAR
    GROUP BY DECODE(DERRIBADO,'R',NOM_IMPERIAL,'I',NOM_REBELDE)
)


--13- Saca cada personaje que m�s ha reclutado.
SELECT PRS.NOMBRE_RECLUTADOR RECLUTADOR_PRO, COUNT(*) RECLUTADOS
FROM PERSONAJES PRS
WHERE PRS.NOMBRE_RECLUTADOR IS NOT NULL -- 
GROUP BY PRS.NOMBRE_RECLUTADOR
HAVING COUNT(*) = (
    SELECT MAX(COUNT(*))
    FROM PERSONAJES PRS2
    WHERE PRS2.NOMBRE_RECLUTADOR IS NOT NULL -- 
    GROUP BY PRS2.NOMBRE_RECLUTADOR
    )
    
--14- El problema m�s repetido
SELECT ACT.PROBLEMA, COUNT(*)
FROM ACTUAR ACT
WHERE ACT.PROBLEMA IS NOT NULL -- HE FILTRADO AQUELLOS QUE NO SON PROBLEMA
AND ACT.PROBLEMA <> 'NINGUNNO' -- 
GROUP BY ACT.PROBLEMA
HAVING COUNT(*) = (
    SELECT MAX(COUNT(*))
    FROM ACTUAR ACT2
    WHERE ACT2.PROBLEMA IS NOT NULL
    AND ACT2.PROBLEMA <> 'NINGUNO'
    GROUP BY ACT2.PROBLEMA
)

--15- El usuario con menos comentarios
SELECT CM.USUARIO, COUNT(*)
FROM COMENTAR CM
WHERE CM.COMENTARIO IS NOT NULL -- QUITO AQUELLOS QUE NO TENGAN COMENTARIOS
-- TAMBI�N PODR�A DEJARSE Y SACAR LOS QUE NO TIENEN COMENTARIOS
GROUP BY CM.USUARIO
HAVING COUNT(*) = (
    SELECT MIN(COUNT(*))
    FROM COMENTAR CM2
    WHERE CM2.COMENTARIO IS NOT NULL -- QUITO AQUELLOS QUE NO HAYAN COMENTADO
    GROUP BY CM2.USUARIO
)

--16- Realiza una vista materializada, que empiece a actualizar a los 5 d�as de su creaci�n y luego
--cada semana, que obtenga alg�n cap�tulo de la saga que no haya tenido comentario alguno o la
--que menos actores haya utilizado y qu�tale los cap�tulos con menos duraci�n.



--17-Obt�n las personajes que hayan visitado alguna localizaci�n y si no lo han hecho tambi�n deben
--salir
SELECT PRS.NOMBRE
FROM PERSONAJES PRS, ACTUAR ACT
WHERE PRS.NOMBRE=ACT.NOM_PERSONAJE(+)
-- SI APARECE EN ACTUACION, APARECE EN ALGUNA LOCALIZACI�N (DE FORMA OBLIGATORIA)
-- POR LO TANTO SI NO APARECE ES QUE NO HAN VISITADO NINGUNA LOCALIZACI�N
GROUP BY PRS.NOMBRE -- QUITANDO DUPLICADOS

--18-Saca localizaciones con m�s de 3 visitas y si hay m�s de una que solo salga la que el c�digo de
--localizaci�n sea menor alfab�ticamente.
-- SOLUCI�N 1
SELECT L.NOMBRE_LOC
FROM LOCALIZACIONES L
WHERE L.COD_LOC = (
    SELECT MIN(LOCALIZACION)
    FROM (
        SELECT ACT.COD_LOC LOCALIZACION
        FROM ACTUAR ACT
        GROUP BY ACT.COD_LOC
        HAVING COUNT(*)>3) -- A VECES VEO M�S INTUITIVO SELECT EN EL FROM
    )
    
-- SOLUCI�N 2
SELECT L.NOMBRE_LOC
FROM LOCALIZACIONES L
WHERE L.COD_LOC = (    
    SELECT MIN(ACT.COD_LOC)
    FROM ACTUAR ACT
    WHERE ACT.COD_LOC IN (
        SELECT ACT2.COD_LOC
        FROM ACTUAR ACT2
        GROUP BY ACT2.COD_LOC
        HAVING COUNT(*)>3
    )
)

--19-Obt�n todos los personajes que no sean ni jedi ni sith.
SELECT PRS.NOMBRE
FROM PERSONAJES PRS
WHERE NOT EXISTS (
    SELECT 1
    FROM SITH S
    WHERE S.NOMBRE_SITH = PRS.NOMBRE
) AND NOT EXISTS (
    SELECT 2
    FROM JEDI J
    WHERE J.NOMBRE_JEDI = PRS.NOMBRE
)

--20-Obt�n los fan que no hayan hecho comentarios y los personajes que salgan en m�s de 1 cap�tulo.
SELECT F.NOMBRE
FROM FANS F
WHERE NOT EXISTS (
    SELECT 1
    FROM COMENTAR C
    WHERE C.USUARIO = F.USUARIO
) -- FANS QUE NO HAYAN HECHO COMENTARIOS
UNION
SELECT ACT.NOM_PERSONAJE
FROM ACTUAR ACT, CAPITULOS CPT
WHERE ACT.CAPITULO = CPT.NUM_CAP
AND ACT.TEMPORADA = CPT.NUM_TEMPO
GROUP BY ACT.NOM_PERSONAJE
HAVING COUNT(*)>1
